Competition (Telegram) Bot
==========================

Funny bot for supporting, let's say, "competitions" among friends with statistic counts and every day notifications. Also can be used for self reminder for important everyday tasks, for example learning foreign language.
Working bot is available at https://telegram.me/haerle_bot.


To install:
-----------

$git clone https://yaugear@bitbucket.org/yaugear/competition_bot.git

$cd competition_bot

$pip install -r requirements.txt


To launch:
----------

1. First, request from [@]botfather TOKEN for your bot and add it to config.py
	
2. Create database (MySQL, PostgreSQL, SQLite and etc.) and provide URL for it in config.py

	MYSQL_URL = 'mysql+pymysql://root:****[@]localhost/competition_bot?charset=utf8'
3. Launch bot's script as:

	$python bot.py


For feedbacks:
--------------

ramil.gata@gmail.com - Ramil Gataullin