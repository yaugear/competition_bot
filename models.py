# -*- coding: UTF-8 -*-
# alter table users convert to character set utf8 collate utf8_unicode_ci;
# alter table competitions convert to character set utf8 collate utf8_unicode_ci;
# alter table items convert to character set utf8 collate utf8_unicode_ci;

import datetime

from sqlalchemy import create_engine, Table, Column, Integer, String, Boolean, Sequence, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from config import MYSQL_URL

engine = create_engine(MYSQL_URL, encoding='utf8')
Session = sessionmaker(bind=engine)
session = Session()
Base = declarative_base()

association_table = Table('user_competitions', Base.metadata,
                          Column('user_id', Integer, ForeignKey('users.id')),
                          Column('competition_id', Integer, ForeignKey('competitions.id'))
                         )

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    uuid = Column(Integer)
    username = Column(String(20))
    state = Column(Integer)
    current_competition_id = Column(Integer, ForeignKey('competitions.id'), nullable=True)
    current_nomination_id = Column(Integer, ForeignKey('items.id'), nullable=True)
    interview_time = Column(Integer)
    lang = Column(String(3))
    current_competition = relationship('Competition', 
                                       foreign_keys='User.current_competition_id')
    current_nomination = relationship('Item', 
                                       foreign_keys='User.current_nomination_id')
    created_by_me = relationship('Competition', 
                                 foreign_keys='Competition.created_by', 
                                 backref='admin', 
                                 lazy='dynamic')
    values = relationship('Value', backref='user', lazy='dynamic')
    competitions = relationship("Competition",
                                secondary=association_table,
                                back_populates="users")

    def __init__(self, uuid, username):
        self.uuid = uuid
        self.username = username
        self.state = 0
        self.interview_time = 18
        self.lang = 'rus'


class Competition(Base):
    __tablename__ = 'competitions'
    id = Column(Integer, Sequence('comp_id_seq'), primary_key=True)
    title = Column(String(200))
    descr = Column(String(200))
    is_active = Column(Boolean)
    start_date = Column(DateTime)
    period_day = Column(Integer)
    current_period = Column(Integer)
    created_by = Column(Integer, ForeignKey('users.id'))
    users = relationship("User",
                         secondary=association_table,
                         back_populates="competitions")
    items = relationship('Item', backref='competition', lazy='dynamic')

    def __init__(self, title, descr, created_by):
        self.title = title
        self.descr = descr
        self.created_by = created_by
        self.is_active = True
        self.start_date = datetime.datetime.now()
        self.period_day = 1
        self.current_period = 0

class Item(Base):
    __tablename__ = 'items'
    id = Column(Integer, Sequence('item_id_seq'), primary_key=True)
    competition_id = Column(Integer, ForeignKey('competitions.id'))
    title = Column(String(20))
    question = Column(String(200))
    points = Column(Integer)
    values = relationship('Value', backref='item', lazy='dynamic')

    def __init__(self, title, question):
        self.title = title
        self.question = question
        self.points = 5

class Value(Base):
    __tablename__ = 'item_values'
    period = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    item_id = Column(Integer, ForeignKey('items.id'), primary_key=True)
    value = Column(Integer)

    def __init__(self, item, value):
        self.period = ((datetime.datetime.now() - item.competition.start_date) / item.competition.period_day).days + 1
        self.item_id = item.id
        self.value = value

Base.metadata.create_all(engine)

# user = User(1, 'Yaugear')
# competition = Competition('тест', 'еще тест', 1)
# item = Item('Surah', 'How many surah do you know?')
# competition.items.append(item)
# item = Item('Pages', 'How many pages do you know?')
# competition.items.append(item)
# user.competitions.append(competition)
# session.add(user)
# session.commit()
