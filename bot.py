# -*- coding: UTF-8 -*-
"""
    APP uses that implementation of bot:
    https://github.com/python-telegram-bot/python-telegram-bot/wiki/Extensions-%E2%80%93-Your-first-Bot


    States:
    1 - initial state
    2 - interviewing. expects numeric answer
    3 - creating competition, expects new competition name
    4 - competition state
    5 -
    6 -
    7 - creating nomination. expects nomination name
    8 - creating nomination. expects nomination question
    9 - changing time. expects hour of day
    10 - creating nomination. expects nomination points
"""

import logging
import datetime
import time

import telegram
from telegram.ext import CommandHandler, Updater, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Job

from config import TOKEN, LANGUAGE_PACK
from models import session, User, Competition, Item, Value, association_table

logging.basicConfig(filename='log.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

def load_lang_pack(filename):
    """
        Loading language pack for localization support
    """
    with open(filename, 'r') as stream:
        data = stream.read().decode('utf-8')
    result = {}
    header = []
    for line in data.split('\n'):
        items = line.split('\t')
        if not header:
            header = items
            continue
        text = dict(zip(header, items))
        result[text['code']] = text

    return result

STRINGS = load_lang_pack(LANGUAGE_PACK)
logging.info("Language packs `%s` length: %s" % (LANGUAGE_PACK, len(STRINGS.keys())))

def gettext(text, lang='eng', params={}):
    if text not in STRINGS:
        return text.replace('\\n', '\n') % params
    else:
        return STRINGS[text].get(lang, STRINGS[text].get('eng', text)).replace('\\n', '\n') % params

def get_keyboard(state):
    if state == 0:
        commands = [['/help', '/back']]
    elif state == 1:
        commands = [['/interview', '/stats', '/help'],
                    ['/create_competition', '/competitions']]
    elif state == 2:
        commands = [['/skip'], ['/help', '/back']]
    elif state == 4:
        commands = [['/add_nomination', '/nominations'], ['/help', '/stats', '/back']]
    elif state == 5:
        commands = [['/users', '/nominations'], ['/help', '/stats', '/back']]
    else:
        commands = [[]]

    return telegram.ReplyKeyboardMarkup(commands, resize_keyboard=True)


def get_current_question(user):
    if isinstance(user, User):
        for competition in user.competitions:
            period_numer = ((datetime.datetime.now() - competition.start_date) / competition.period_day).days + 1
            for item in competition.items:
                value = item.values.order_by(Value.item_id)\
                                   .filter(Value.user_id == user.id)\
                                   .filter(Value.period == period_numer)\
                                   .first()
                if not value:
                    return item
    return None

def login(uuid, username=None):
    """
        Login user
    """
    user = session.query(User).filter(User.uuid == uuid).first()
    if user:
        if username and user.username != username:
            user.username = username
            session.commit()
    else:
        user = session.query(User).filter(User.username == username).first()
        if user:
            user.uuid = uuid
            user.state = 1
            session.commit()
        else:
            user = User(uuid=uuid, username=username)
            user.state = 1
            session.add(user)
            session.commit()
            logging.info("New user(id=%s, uuid=%s, username=%s)" % (user.id,
                                                                    user.uuid,
                                                                    user.username))

    return user


def start(bot, update):
    """
        Starting bot
    """
    user = login(update.message.from_user.id, update.message.from_user.username)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("hello",
                                 lang=user.lang,
                                 params={'username': user.username}))
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("info", lang=user.lang),
                    reply_markup=get_keyboard(state=1))


def unknown(bot, update):
    """
        Handles unknown commands
    """
    user = login(update.message.from_user.id, update.message.from_user.username)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("error_dont_know_command", lang=user.lang))


def help(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state == 4 and user.current_competition:
        if user.current_competition.created_by == user.id:
            message = gettext("competition_info2",
                              lang=user.lang,
                              params={'comp_title': user.current_competition.title})
        else:
            message = gettext("competition_info1",
                              lang=user.lang,
                              params={'comp_title': user.current_competition.title,
                                      'username': user.current_competition.admin.username})
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message,
                        reply_markup=get_keyboard(4))
    elif user.state == 3:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("help2", lang=user.lang),
                        reply_markup=get_keyboard(0))
    else:
        if user.state != 1:
            user.state = 1
            session.commit()
        message = gettext("help1", lang=user.lang)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message,
                        reply_markup=get_keyboard(1))


def back(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state in [1, 2, 3, 4, 9]:
        user.state = 1
        user.current_competition_id = None
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("back", lang=user.lang),
                        reply_markup=get_keyboard(1))
    elif user.state in [5, 6, 7, 8, 10]:
        user.state = 4
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("back", lang=user.lang),
                        reply_markup=get_keyboard(4))
    else:
        user.state = 1
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("back", lang=user.lang),
                        reply_markup=get_keyboard(1))


def change_time(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    user.state = 9
    session.commit()
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("set_time", lang=user.lang,
                                 params={'time': user.interview_time}),
                    reply_markup=get_keyboard(0))


def change_language(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state != 1:
        user.state = 1
        session.commit()
    message = gettext("languages_list", lang=user.lang)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=message,
                    reply_markup=get_keyboard(1))


def choose_russian(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    user.lang = 'rus'
    session.commit()
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("hello",
                                 lang=user.lang,
                                 params={'username': user.username}),
                    reply_markup=get_keyboard(1))

def choose_english(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    user.lang = 'eng'
    session.commit()
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("hello",
                                 lang=user.lang,
                                 params={'username': user.username}),
                    reply_markup=get_keyboard(1))

def choose_tatar(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    user.lang = 'tat'
    session.commit()
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("hello",
                                 lang=user.lang,
                                 params={'username': user.username}),
                    reply_markup=get_keyboard(1))


def interview(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    item = get_current_question(user)
    if item:
        user.state = 2
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text='`%s`: %s' % (item.competition.title, item.question),
                        reply_markup=get_keyboard(2))
    else:
        user.state = 1
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("interview_is_done", lang=user.lang),
                        reply_markup=get_keyboard(1))


def skip_question(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state == 2:
        item = get_current_question(user)
        value = Value(item, 0)
        user.values.append(value)
        session.commit()

        item = get_current_question(user)
        if item:
            # send next question
            bot.sendMessage(chat_id=update.message.chat_id,
                            text='`%s`: %s' % (item.competition.title, item.question),
                            reply_markup=get_keyboard(2))
        else:
            # all items are done
            user.state = 1
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("data_collected",
                                         lang=user.lang),
                            reply_markup=get_keyboard(1))


def show_competitions_list(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state != 1:
        user.state = 1
        session.commit()
    message = gettext("competition_list", lang=user.lang)
    keyboard = [[InlineKeyboardButton(comp.title, callback_data='competition|%s' % comp.id),
                 InlineKeyboardButton(gettext("delete", lang=user.lang),
                                      callback_data='command|delete_comp|%s' % comp.id)
                 if comp.created_by == user.id else
                 InlineKeyboardButton(gettext("leave", lang=user.lang),
                                      callback_data='command|leave_comp|%s' % comp.id)]
                for comp in user.competitions]
    keyboard += [[InlineKeyboardButton(gettext("create_competition", lang=user.lang),
                                       callback_data='command|create_competition')]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=message,
                    reply_markup=reply_markup)


def create_competition(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    user.state = 3
    session.commit()
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("quest_competition_name", lang=user.lang),
                    reply_markup=get_keyboard(0))


def show_nominations_list(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    competition = user.current_competition
    if competition:
        if user.state != 4:
            user.state = 4
            session.commit()
        message = gettext("nomination_list",
                          lang=user.lang,
                          params={'comp_title': competition.title})
        if competition.created_by == user.id:
            keyboard = [[InlineKeyboardButton(item.title, callback_data='nomination|%s' % item.id),
                         InlineKeyboardButton(gettext("delete", lang=user.lang),
                                              callback_data='command|delete_nom|%s' % item.id)]
                        for item in competition.items]
            keyboard += [[InlineKeyboardButton(gettext("add_nomination", lang=user.lang),
                                               callback_data='command|add_nomination')]]
            reply_markup = InlineKeyboardMarkup(keyboard)
        else:
            message += '\n-'.join(['%s: %s*' % (item.title, item.points)
                                   for item in competition.items])
            reply_markup = get_keyboard(4)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message,
                        reply_markup=reply_markup)
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_with_competition1",
                                     lang=user.lang),
                        reply_markup=get_keyboard(1))


def create_nomination(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.current_competition:
        if user.current_competition.created_by == user.id:
            user.state = 7
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("quest_nomination_name",
                                         lang=user.lang,
                                         params={'comp_title': user.current_competition.title}),
                            reply_markup=get_keyboard(0))
        else:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("error_with_permission3",
                                         lang=user.lang,
                                         params={'comp_title': user.current_competition.title}),
                            reply_markup=get_keyboard(5))

    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_with_competition1",
                                     lang=user.lang),
                        reply_markup=get_keyboard(1))


def show_users_list(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.current_competition:
        if user.state != 4:
            user.state = 4
            session.commit()
        message = gettext("users_list",
                          lang=user.lang,
                          params={'comp_title': user.current_competition.title})
        message += '\n@'.join(sorted([u.username for u in user.current_competition.users]))
        if user.current_competition.created_by == user.id:
            message += gettext("help3", lang=user.lang)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message,
                        reply_markup=get_keyboard(4))
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_with_competition1", lang=user.lang),
                        reply_markup=get_keyboard(1))


def add_user(bot, update, args):
    user = login(update.message.from_user.id, update.message.from_user.username)
    competition = user.current_competition
    if competition:
        if competition.created_by == user.id:
            if len(args) > 0:
                if user.state != 4:
                    user.state = 4
                user_exists = session.query(User).filter(User.username == args[0]).first()
                if not user_exists:
                    new_user = User(0, args[0])
                    session.add(new_user)
                    new_user.competitions.append(competition)
                    session.commit()
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=gettext("user_have_been_added2",
                                                 lang=user.lang,
                                                 params={'username': args[0],
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
                elif user_exists not in competition.users:
                    user_exists.competitions.append(competition)
                    session.commit()
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=gettext("user_have_been_added1",
                                                 lang=user.lang,
                                                 params={'username': args[0],
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
                    # send message to added user about it
                    bot.sendMessage(chat_id=user_exists.uuid,
                                    text=gettext("you_have_been_added_to_competition",
                                                 lang=user_exists.lang,
                                                 params={'username': user.username,
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(1))
                else:
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=gettext("error_with_add_user2", lang=user.lang,
                                                 params={'username': args[0],
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
            else:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=gettext("error_with_add_user",
                                             lang=user.lang),
                                reply_markup=get_keyboard(4))
        else:
            reply_markup = telegram.ReplyKeyboardMarkup()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("error_with_permission1", lang=user.lang),
                            reply_markup=get_keyboard(4))
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_with_competition1", lang=user.lang),
                        reply_markup=get_keyboard(1))

def delete_user(bot, update, args):
    user = login(update.message.from_user.id, update.message.from_user.username)
    competition = user.current_competition
    if competition:
        if competition.created_by == user.id:
            if len(args) > 0:
                if user.state != 4:
                    user.state = 4
                user_exists = session.query(User).filter(User.username == args[0]).first()
                if user_exists and user_exists in competition.users:
                    competition.users.remove(user_exists)
                    session.commit()
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=gettext("user_have_been_removed", lang=user.lang,
                                                 params={'username': args[0],
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
                    # send message to deleted user about it
                    bot.sendMessage(chat_id=user_exists.uuid,
                                    text=gettext("you_have_been_removed_from_competition",
                                                 lang=user_exists.lang,
                                                 params={'username': user.username,
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(1))
                else:
                    bot.sendMessage(chat_id=update.message.chat_id,
                                    text=gettext("error_with_delete_user2",
                                                 lang=user.lang,
                                                 params={'username': args[0],
                                                         'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
            else:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=gettext("error_with_delete_user1",
                                             lang=user.lang),
                                reply_markup=get_keyboard(4))
        else:
            reply_markup = telegram.ReplyKeyboardMarkup()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("error_with_permission2", lang=user.lang),
                            reply_markup=get_keyboard(4))
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_with_competition1", lang=user.lang),
                        reply_markup=get_keyboard(1))

def show_stats(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state in [1, 2, 3]:
        longest_comp = session.query(Competition).filter(Competition.users.any(User.id == user.id))\
                                                 .order_by(Competition.start_date).first()
        if not longest_comp:
            stats = {'days_count': 0, 
                     'comp_count': 0, 
                     'nom_count': 0,
                     'user_count': 0,
                     'allpoints': 0}
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("stats", lang=user.lang, params=stats),
                            reply_markup=get_keyboard(1))
        else:
            nom_count = 0
            allpoints = 0
            result = []
            allusers = []
            for competition in user.competitions:
                nom_count += len(competition.items.all())
                points = 0
                for nomination in competition.items.all():
                    values = session.query(Value).filter(Value.item_id == nomination.id)\
                                                 .filter(Value.user_id == user.id).all()
                    for value in values:
                        points += value.value * value.item.points
                allpoints += points
                result.append((competition.title, points))
                allusers += competition.users
            result.sort(key=lambda tup: tup[0])
            stats = {'days_count': (datetime.datetime.now() - longest_comp.start_date).days + 1, 
                     'comp_count': len(user.competitions), 
                     'nom_count': nom_count,
                     'user_count': len(set(allusers)),
                     'allpoints': allpoints}
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("stats", lang=user.lang, params=stats))

            result.sort(key=lambda tup: tup[0])
            message = gettext("stats_comp_list", lang=user.lang)
            message += '\n-'.join(['`%s`: %s' % row for row in result])
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=message,
                            reply_markup=get_keyboard(1))



    elif user.state in [4, 5, 6, 7, 8, 9, 10]:
        if user.current_competition:
            result = []
            for comp_user in user.current_competition.users:
                points = 0
                for nomination in user.current_competition.items.all():
                    values = session.query(Value).filter(Value.item_id == nomination.id)\
                                                 .filter(Value.user_id == comp_user.id).all()
                    for value in values:
                        points += value.value * value.item.points
                result.append((comp_user.username, points))
            result.sort(key=lambda tup: -tup[1])
            message = gettext("best_users_list", lang=user.lang)
            message += '\n'.join(['@%s\t%s' % row for row in result])
            if user.current_competition.created_by == user.id:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=message,
                                reply_markup=get_keyboard(4))
            else:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=message,
                                reply_markup=get_keyboard(5))
        else:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("error_with_competition1", lang=user.lang),
                            reply_markup=get_keyboard(1))
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_dont_know_command", lang=user.lang))




def echo(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state == 2:
        # interviewing
        item = get_current_question(user)
        try:
            value = Value(item, int(update.message.text))
            user.values.append(value)
            session.commit()
        except ValueError:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("error_with_interview", lang=user.lang))
        item = get_current_question(user)
        if item:
            # send next question
            bot.sendMessage(chat_id=update.message.chat_id,
                            text='`%s`: %s' % (item.competition.title, item.question),
                            reply_markup=get_keyboard(2))
        else:
            # all items are done
            user.state = 1
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("data_collected", lang=user.lang),
                            reply_markup=get_keyboard(1))
    elif user.state == 3:
        user.state = 4
        competition = Competition(update.message.text, "", user.id)
        user.competitions.append(competition)
        session.commit()
        user.current_competition_id = competition.id
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("competition_created", lang=user.lang,
                                     params={'comp_title': competition.title}),
                        reply_markup=get_keyboard(4))
    elif user.state == 7:
        # adding new nomination
        competition = user.current_competition
        if competition:
            user.state = 10
            nomination = Item(update.message.text, update.message.text + " ?")
            competition.items.append(nomination)
            session.commit()
            user.current_nomination_id = nomination.id
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("quest_nomination_point",
                                         lang=user.lang,
                                         params={'nom_title': nomination.title}),
                            reply_markup=get_keyboard(0))
    elif user.state == 8:
        nomination = session.query(Item).get(user.current_nomination_id)
        if nomination:
            nomination.question = update.message.text
            user.state = 4
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("nomination_created", lang=user.lang,
                                         params={'nom_title': nomination.title}),
                            reply_markup=get_keyboard(4))
    elif user.state == 10:
        nomination = session.query(Item).get(user.current_nomination_id)
        if nomination:
            try:
                nomination.points = int(update.message.text)
                user.state = 8
                session.commit()
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=gettext("quest_nomination_quest", lang=user.lang,
                                             params={'nom_title': nomination.title}),
                                reply_markup=get_keyboard(0))
            except ValueError, err:
                bot.sendMessage(chat_id=update.message.chat_id,
                                text=gettext("error_with_point_value", lang=user.lang),
                                reply_markup=get_keyboard(0))

    elif user.state == 9:
        if update.message.text in ["0", "1", "2", "3", "4", "5", "6", "7",
                                   "8", "9", "10", "11", "12", "13", "14", "15",
                                   "16", "17", "18", "19", "20", "21", "22", "23"]:
            user.state = 1
            user.interview_time = int(update.message.text)
            session.commit()
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("new_time_is_set",
                                         lang=user.lang,
                                         params={'time': update.message.text}),
                            reply_markup=get_keyboard(1))
        else:
            message = gettext("error_with_time",
                              lang=user.lang,
                              params={'value': update.message.text})
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=message,
                            reply_markup=get_keyboard(0))

    else:
        bot.sendMessage(chat_id=update.message.chat_id, text='%s ?' % update.message.text)


def button(bot, update):
    query = update.callback_query
    bot.answerCallbackQuery(callback_query_id=query.id)
    parameters = query.data.split('|')
    user = login(query.message.chat_id)
    if parameters[0] == 'competition':
        competition = session.query(Competition).get(parameters[1])
        if competition:
            if user in competition.users:
                user.state = 4
                user.current_competition_id = competition.id
                session.commit()
                if competition.created_by == user.id:
                    bot.sendMessage(chat_id=query.message.chat_id,
                                    text=gettext("competition_info3",
                                                 lang=user.lang,
                                                 params={'comp_title': competition.title}),
                                    reply_markup=get_keyboard(4))
                else:
                    bot.sendMessage(chat_id=query.message.chat_id,
                                    text=gettext("competition_info3",
                                                 lang=user.lang,
                                                 params={'comp_title': competition.title}),
                                    reply_markup=get_keyboard(5))
            else:
                bot.sendMessage(chat_id=query.message.chat_id,
                                text=gettext("error_with_competition2", lang=user.lang),
                                reply_markup=get_keyboard(1))
        else:
            bot.sendMessage(chat_id=query.message.chat_id,
                            text=gettext("error_with_competition2", lang=user.lang),
                            reply_markup=get_keyboard(1))
    elif parameters[0] == 'nomination':
        nomination = session.query(Item).get(parameters[1])
        user.state = 10
        user.current_nomination_id = nomination.id
        session.commit()
        bot.sendMessage(chat_id=query.message.chat_id,
                        text=gettext("edit_nomination",
                                     lang=user.lang,
                                     params={'nom_title': nomination.title,
                                             'nom_quest': nomination.question,
                                             'nom_points': nomination.points}),
                        reply_markup=get_keyboard(0))
    elif parameters[0] == 'command':
        if parameters[1] == 'create_competition':
            user.state = 3
            session.commit()
            bot.sendMessage(chat_id=query.message.chat_id,
                            text=gettext("quest_competition_name", lang=user.lang),
                            reply_markup=get_keyboard(0))
        elif parameters[1] == 'delete_comp':
            competition = session.query(Competition).get(parameters[2])
            if competition and competition.created_by == user.id:
                user.competitions.remove(competition)
                session.delete(competition)
                session.commit()
                bot.editMessageText(chat_id=query.message.chat_id,
                                    text=gettext("competition_deleted",
                                                 lang=user.lang,
                                                 params={'comp_title': competition.title}),
                                    message_id=query.message.message_id)
        elif parameters[1] == 'leave_comp':
            competition = session.query(Competition).get(parameters[2])
            if competition:
                user.competitions.remove(competition)
                session.commit()
                bot.editMessageText(chat_id=query.message.chat_id,
                                    text=gettext("you_left_competition",
                                                 lang=user.lang,
                                                 params={'comp_title': competition.title,
                                                         'username': competition.created_by.username}),
                                    message_id=query.message.message_id)
        elif parameters[1] == 'add_nomination':
            competition = session.query(Competition).filter(Competition.id == user.current_competition_id)\
                                     .filter(Competition.users.any(User.id == user.id)).first()
            if competition:
                user.state = 7
                session.commit()
                bot.sendMessage(chat_id=query.message.chat_id,
                                text=gettext("quest_nomination_name",
                                             lang=user.lang,
                                             params={'comp_title': competition.title}),
                                reply_markup=get_keyboard(0))
        elif parameters[1] == 'delete_nom':
            nomination = session.query(Item).get(parameters[2])
            if nomination and nomination.competition.created_by == user.id:
                users = session.query(User).filter(User.current_nomination_id == nomination.id).all()
                for usr in users:
                    usr.current_nomination_id = None
                competition = nomination.competition
                nomination.competition.items.remove(nomination)
                for value in nomination.values:
                    session.delete(value)
                session.delete(nomination)
                session.commit()
                bot.editMessageText(text=gettext("nomination_deleted",
                                                 lang=user.lang,
                                                 params={'nom_title': nomination.title}),
                                    chat_id=query.message.chat_id,
                                    message_id=query.message.message_id)


def periodic_interview(bot, job):
    """
        Handles the periodic interviews
    """
    users = session.query(User)\
                   .filter(User.interview_time == int(datetime.datetime.now().strftime('%H')) + 3)
    for user in users:
        item = get_current_question(user)
        if item:
            user.state = 2
            session.commit()
            bot.sendMessage(chat_id=user.uuid,
                            text=gettext("start_interview", lang=user.lang))
            bot.sendMessage(chat_id=user.uuid,
                            text='`%s`: %s' % (item.competition.title, item.question),
                            reply_markup=get_keyboard(state=2))

    # wrapping unanswered question as null
    users = session.query(User)\
                   .filter(User.interview_time == int(datetime.datetime.now().strftime('%H')) + 1)
    for user in users:
        item = get_current_question(user)
        if item:
            user.state = 1
            session.commit()
            bot.sendMessage(chat_id=user.uuid,
                            text=gettext("wrapping_interview", lang=user.lang))

def main():
    updater = Updater(token=TOKEN)

    job_minute = Job(periodic_interview, 3600)
    jober = updater.job_queue
    jober.put(job_minute, next_t=0.0)

    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('back', back))
    dispatcher.add_handler(CommandHandler('change_time', change_time))
    dispatcher.add_handler(CommandHandler('change_language', change_language))
    dispatcher.add_handler(CommandHandler('russian', choose_russian))
    dispatcher.add_handler(CommandHandler('english', choose_english))
    dispatcher.add_handler(CommandHandler('tatar', choose_tatar))
    dispatcher.add_handler(CommandHandler('interview', interview))
    dispatcher.add_handler(CommandHandler('skip', skip_question))
    dispatcher.add_handler(CommandHandler('competitions', show_competitions_list))
    dispatcher.add_handler(CommandHandler('create_competition', create_competition))
    dispatcher.add_handler(CommandHandler('users', show_users_list))
    dispatcher.add_handler(CommandHandler('nominations', show_nominations_list))
    dispatcher.add_handler(CommandHandler('add_nomination', create_nomination))
    dispatcher.add_handler(CommandHandler('add_user', add_user, pass_args=True))
    dispatcher.add_handler(CommandHandler('delete_user', delete_user, pass_args=True))
    dispatcher.add_handler(CommandHandler('stats', show_stats))

    dispatcher.add_handler(MessageHandler([Filters.command], unknown))
    dispatcher.add_handler(MessageHandler([Filters.text], echo))

    updater.dispatcher.add_handler(CallbackQueryHandler(button))

    updater.start_polling()

if __name__ == '__main__':
    main()
