import os

MYSQL_URL = 'mysql+pymysql://root:****@localhost/competition_bot?charset=utf8'
TOKEN = 'MyToken'

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
LANGUAGE_PACK = os.path.join(BASE_DIR, 'strings.tsv')
